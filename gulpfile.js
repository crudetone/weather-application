'use strict'

var gulp = require('gulp'),
	webServer = require('gulp-webserver'),
	sass = require('gulp-sass');

gulp.task('js', function(){
	gulp.src('js/**/.js')
});
gulp.task('html', function(){
	gulp.src('views/**./*.html')
});
gulp.task('sass', function(){
	gulp
		.src('sass/*.scss')
		.pipe(sass())
		.pipe(gulp.dest('css/'))
});
gulp.task('watch', function(){
	gulp.watch('js/**/*.js', ['js']);
	gulp.watch('html/**/*.html', ['html']);
	gulp.watch('sass/*.scss', ['sass']);
})
gulp.task('webServer', function(){
	gulp
		.src('./')
		.pipe(webServer({
			livereload: true,
			port: 9000,
			open: true
		}));
})
gulp.task('start', [
	'html',
	'js',
	'sass',
	'webServer',
	'watch'
]);