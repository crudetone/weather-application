angular
	.module('homeController', ['uiGmapgoogle-maps', 'nemLogging'])
	.controller('homeCtrl', ['$scope', 'users', 'weatherService', function($scope, users, weatherService){
		$scope.title = 'Select user';
		$scope.users = users;
		$scope.userDetails = function(item) {
			$scope.selected = item;
			$scope.showDetails = true

			$scope.map = {
				center: {
					latitude: $scope.selected.lat,
					longitude: $scope.selected.long
				},
				zoom: 10
			}
			$scope.marker = {
				id: $scope.selected.id,
				coords: {
					latitude: $scope.selected.lat,
					longitude: $scope.selected.long
				},
				options: { draggable: false }
			};
			var city = $scope.selected.city
			function fetchWeather(city) {
        weatherService.getWeather(city).then(function(data){
          $scope.place = data;
        }); 
      }
      fetchWeather(city);
		}
	}])

	.factory('weatherService', ['$http', '$q', function ($http, $q){
    function getWeather (city) {
      var deferred = $q.defer();
      $http.get('https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22'+ city +'%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys')
        .success(function(data){
          deferred.resolve(data.query.results.channel);
        })
        .error(function(err){
          console.log('Error retrieving markets');
          deferred.reject(err);
        });
      return deferred.promise;
    }
    return {
      getWeather: getWeather
    };
  }]);