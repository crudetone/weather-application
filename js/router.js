angular
	.module('router', [
		'ui.router'
	])
	.config(['$urlRouterProvider', '$stateProvider', '$locationProvider', function($urlRouterProvider, $stateProvider, $locationProvider){
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home', {
				url: '/',
				templateUrl: '../views/users.html',
				controller: 'homeCtrl',
				resolve: {
					users: ['$http', function($http){
						var users = $http.get('../users.json');
						return users.then(function(res){
							return res.data
						})
					}]
				}
			})
		$locationProvider.html5Mode({enabled: true, requireBase: false})
	}])